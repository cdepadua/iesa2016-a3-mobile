<?php
    ini_set('display_errors', 1);
    error_reporting(E_ALL);

    include '../global.php';

    //Connection a la base SQL
    $mysqli = new mysqli($host, $db_username, $db_password, $db_name);

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    //Recuperation du formulaire d'inscription
    //Informations obligatoires
    $nickname               =   isset($_POST["user_name"])              ? addslashes($_POST["user_name"])             : "";
    $firstname              =   isset($_POST["user_firstname"])         ? addslashes($_POST["user_firstname"])        : "";
    $lastname               =   isset($_POST["user_lastname"])          ? addslashes($_POST["user_lastname"])         : "";
    $email                  =   isset($_POST["email"])                  ? addslashes($_POST["email"])                 : "";
    $password               =   isset($_POST["user_pass"])              ? sha1($_POST["user_pass"])                   : "";
    $create_at              =   date('d-m-Y');

    //Champs facultatifs
    $birthday               =   isset($_POST["birthday"])               ? ($_POST["birthday"])                        : null;

    //Verifie si les champs sont vides
    if (empty($nickname) || empty($firstname) || empty($lastname) || empty($email) || empty($password) || empty($password_confirm)) {
        //Champs vides
        http_response_code(400);
    } else {
        //Verifie si l'email est valide
        //$request    =   $mysqli->prepare("SELECT * FROM $table_users WHERE $column_email= :email");
        //$request->execute(array('email'=>$email));
        // $json   =   json_encode($assoc);
        //echo $json . PHP_EOL;
        $request =  $mysqli->prepare( '
                    INSERT INTO fruits( $nickname_column, $firstname_column, $lastname_column, $email_column, $password_column, $birthday_column, $create_at_column )
                    VALUES( ":nickname", ":firstname", ":lastname", ":email", ":password", ":birthday", ":create_at" )
                    ');
        $param = array(
                    "nickname"  =>  $nickname,
                    "firstname" =>  $firstname,
                    "lastname"  =>  $lastname,
                    "email"     =>  $email,
                    "password"  =>  $password,
                    "birthday"  =>  $birthday,
                    "create_at" =>  $create_at
        );
        if($request->execute($param)){

        }
    }

    
?>
